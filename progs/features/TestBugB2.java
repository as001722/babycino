class Main {
    public static void main(String[] args){
		System.out.println(new TestBugB2().Start());
	}
}


class TestBugB2 {
	public int Start() {
		boolean a;
		boolean b;

		a = true;
		b = false;

		if (a != b) {
			System.out.println(1);
		} else {
			System.out.println(0);
		}

    //Added return for successfull compile to test Bug
    return 0;

	}
}
